package ninja.skeletonkey;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main {



    public static void main(String[] args) {
        String x = System.getProperty("user.home") + "/Desktop";
        writeFile(x,"ED0006");

    }

    private static void writeFile(String path,String cont){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            String content = cont;
            String outputPath = path+"\\"+cont+".txt";
            System.out.println("Writing to "+outputPath);

            fw = new FileWriter(outputPath);
            bw = new BufferedWriter(fw);
            bw.write(content);

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

    }
}

